<?php 

function calcul($num1 = 0, $num2 = 1, $num3 = 3){
  if(is_int($num1) && is_int($num2) && is_int($num3)){
    return $num1 + $num2 + $num3;
  }else {
    return "Veuillez ajouter trois chiffres dans cette fonction";
  }
}

echo calcul(1, 4, 5);
echo "\n" . calcul("coucou", 4, 5);
echo "\n" . calcul();
 ?>