<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>on fait joujou avec php</title>
  </head>
  <body>
    
    <!-- Exercice 1 -->
  
      <?php 
      if(isset($_GET['nom']) && isset($_GET['prenom'])){
      ?>
      <section>
      <p>Bonjour <?php echo ($_GET['prenom']);?> <?= $_GET['nom']; ?></p>
      </section>
      <?php  
      }
      ?>
      <section>
        <p>
          <?php 
            if(isset($_GET['nom']) && isset($_GET['prenom'])){
              echo "Bonjour " . $_GET['nom'] . " " . $_GET['prenom'];
            }
          
           ?>
        </p>
      </section>
    <!-- Exercice 2 -->
      <section>
          <p>
            
            <?php 
            if(isset($_GET['age'])){
              if ($_GET['age'] != "") {
                echo "Bonjour " . $_GET['prenom'] . " " . $_GET['nom'] . ", tu as ". $_GET['age'] . " ans";
              }else {
                echo "Bonjour " . $_GET['prenom'] . " " . $_GET['nom'] . ". Je pourrai te dire ton age si tu me le donnais";
              }
            }
             ?>
            
          </p>
      </section>
    
    <!-- Exercice 3 -->
    
      <section>
        <p>
          <?php 
            if (isset($_GET['dateDebut']) && isset($_GET['dateFin']) ) {
              echo "date de Début: " . $_GET['dateDebut'] . " ,date de Fin: ". $_GET['dateFin'] ;
            }
           ?>
        </p>
      </section>
    
    <!-- Exercice 4 -->
    
    <section>
      <p>
        <?php 
          if (isset($_GET['langage']) && isset($_GET['serveur']) ) {
            echo "language: " .$_GET['langage'] . " \n serveur: " . $_GET['serveur'] ;
          }
         ?>
      </p>
    </section>
    
    <!-- Exercice 5 -->
    
    <section>
      <p>
        <?php 
          if (isset($_GET['semaine'])) {
            echo "semaine: " .$_GET['semaine'] ;
          }
         ?>
      </p>
    </section>
    
    <!-- Exercice 6 -->
    <section>
      <p>
        <?php 
          if (isset($_GET['batiment']) && isset($_GET['salle']) ) {
            echo "batiment: " .$_GET['batiment'] . " \n salle: " . $_GET['salle'] ;
          }
         ?>
      </p>
    </section>
    
  </body>
</html>