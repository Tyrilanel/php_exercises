<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>On fait des trucs avec PHP!</title>
  </head>
  <body>
    <?php if (empty($_POST)){
      echo ("
      <section>
        <form action='index.php' method='post'>
        <select name='gender'>
          <option value='Monsieur'>Monsieur</option>
          <option value='Madame'>Madame</option>
        </select>
        <input type='text' name='nom' placeholder='Nom' required>
        <input type='text' name='prenom' placeholder='Prenom' required>
        <input type='file' name='file' required>
        <button type='submit'>Valider</button>
        </form>
      </section>");
    } else {
      $file = pathinfo($_POST['file']);
      if($file['extension'] == "pdf"){
      echo("<section>
        <p>
          Bonjour ". $_POST['gender'] ." " . $_POST['prenom'] . " " . $_POST['nom']." <br> nom du fichier : " . $file['filename'] . "<br>". "extention du fichier: " . $file['extension'] . " 
        </p>
      </section>
      ");
    }else {
      echo ("<section>
        <p>
          Bonjour ". $_POST['gender'] ." " . $_POST['prenom'] . " " . $_POST['nom'] ." <br> désolé, votre fichier n'est pas un format PDF.
            </p>
          </section>
          ");
    }
    } ?>
  </body>
</html>